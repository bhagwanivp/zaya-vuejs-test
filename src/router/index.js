import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/:lessonId',
    name: 'Lesson',
    component: Dashboard
  },
  {
    path: '/:lessonId/:objectiveId',
    name: 'Objective',
    component: Dashboard
  },
  {
    path: '/:lessonId/:objectiveId/:videoId',
    name: 'Video',
    component: Dashboard
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
