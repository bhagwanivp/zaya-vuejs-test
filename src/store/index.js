import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'

Vue.use(Vuex)
Vue.use(VueAxios, axios)

Vue.axios.defaults.baseURL = 'https://api.myjson.com/bins/'

const state = {
  recitalTitle: '',
  instrumentTitle: '',
  lessonDetails: [],
  currentLesson: undefined,
  currentObjective: undefined,
  currentVideo: undefined,
  thumbnails: [],
  isLoading: false
}

const getters = {
  selectedLesson: state => {
    if (!state.lessonDetails) return {}
    return (
      state.lessonDetails.find(lesson => lesson.id === state.currentLesson) ||
      {}
    )
  },
  selectedObjective: (state, getters) => {
    const lessonDetail = getters.selectedLesson
    if (!lessonDetail.objectiveDetails) return {}
    return (
      lessonDetail.objectiveDetails.find(
        obj => obj.id === state.currentObjective
      ) || {}
    )
  },
  selectedVideo: (state, getters) => {
    const objectiveDetail = getters.selectedObjective
    if (!objectiveDetail.objectiveVideosDetails) return {}
    return (
      objectiveDetail.objectiveVideosDetails.find(
        videoObj => Number(videoObj.id) === Number(state.currentVideo)
      ) || {}
    )
  },
  getThumbnailUrl: state => videoId => {
    const found = state.thumbnails.find(video => video.id === videoId)
    return found ? found.thumbnail_url_with_play_button : ''
  }
}

const actions = {
  loadRecital({ commit }) {
    commit('SET_LOADING', true)
    Vue.axios
      .get('qubzl')
      .then(result => {
        commit('SAVE_RECITAL', result.data)
        commit('SET_LOADING', false)
      })
      .catch(error => {
        throw new Error(`API ${error}`)
      })
  },
  loadThumbnails({ commit, getters }) {
    const obj = getters.selectedObjective
    Object.keys(obj).length !== 0 &&
      obj.constructor === Object &&
      obj.objectiveVideosDetails.forEach(video => {
        axios
          .get(
            'https://vimeo.com/api/oembed.json?url=' +
              encodeURIComponent(video.url)
          )
          .then(res => {
            commit('SAVE_THUMBNAILS', { ...video, ...res.data })
          })
      })
  }
}

const mutations = {
  SAVE_RECITAL(state, recital) {
    state.recitalTitle = recital.recitalTitle
    state.instrumentTitle = recital.instrumentTitle
    state.lessonDetails = recital.lessonDetails
  },
  SAVE_LESSON_SELECTION(state, id) {
    state.currentLesson = id
  },
  SAVE_OBJECTIVE_SELECTION(state, id) {
    state.currentObjective = id
  },
  SAVE_VIDEO_SELECTION(state, id) {
    state.currentVideo = id
  },
  SAVE_THUMBNAILS(state, data) {
    state.thumbnails.push(data)
  },
  SET_OBJECTIVE_STATUS(state, { index, objective }) {
    let i
    for (i = 0; i < state.lessonDetails.length; i++) {
      const lesson = state.lessonDetails[i]

      if (lesson.objectiveDetails[index].id === objective.id) {
        break
      }
    }
    if (i === state.lessonDetails.length) {
      console.log('error: objective not found, cannot set status')
      return
    }
    state.lessonDetails[i].objectiveDetails[index] = objective
  },
  SET_LOADING(state, loadingStatus) {
    state.isLoading = loadingStatus
  },
  RESET_PROGRESS(state) {
    state.currentLesson = undefined
    state.currentObjective = undefined
    state.currentVideo = undefined

    for (let i = 0; i < state.lessonDetails.length; i++) {
      state.lessonDetails[i].objectiveDetails.forEach(obj => {
        obj.status = ''
      })
    }
  }
}

const modules = {}

export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
  modules
})
